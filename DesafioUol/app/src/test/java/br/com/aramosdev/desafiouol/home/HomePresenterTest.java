package br.com.aramosdev.desafiouol.home;

import org.junit.Test;
import org.mockito.Mock;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.aramosdev.desafiouol.base.BaseUnitTest;
import br.com.aramosdev.desafiouol.base.UnitTestUtil;
import br.com.aramosdev.desafiouol.core.BaseContract;
import br.com.aramosdev.desafiouol.model.shots.Shot;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by Alberto.Ramos on 25/09/17.
 */

public class HomePresenterTest extends BaseUnitTest<List<Shot>> {

    @Mock
    private HomeContract.View mView;

    private HomeContract.Interaction mPresenter;
    private List<Shot> mResponse;

    @Override
    public void setup() {
        super.setup();
    }

    @Test
    public void testGetShots_responseNull() throws Exception {
        setupMockedResponse(null);
        mPresenter.getShots();
        verify(mView).showLoading();
        verify(mView).hideLoading();
        verify(mView, never()).fillShotList((List<Shot>) any());
    }

    @Test
    public void testGetShots_responseEmpty() throws Exception {
        setupMockedResponse(new ArrayList<Shot>(0));
        mPresenter.getShots();
        verify(mView).showLoading();
        verify(mView).hideLoading();
        verify(mView, never()).fillShotList((List<Shot>) any());
    }


    @Test
    public void testGetShots_success() throws Exception {
        mPresenter.getShots();
        verify(mView).showLoading();
        verify(mView).hideLoading();
        verify(mView).fillShotList(eq(mResponse));
    }

    @Override
    protected BaseContract.BaseInteraction<List<Shot>> getPresenter() {
        mPresenter = new HomePresenter(mApi, mView);
        return mPresenter;
    }

    @Override
    protected List<Shot> getFullResponse() {
        Type type = getPresenter().genericType();
        mResponse = UnitTestUtil.getInstance().readFile(type, "shot_list.json");
        return mResponse;
    }
}
