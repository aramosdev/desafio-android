package br.com.aramosdev.desafiouol.shotlist;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.aramosdev.desafiouol.base.UnitTestUtil;
import br.com.aramosdev.desafiouol.model.shots.Shot;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by Alberto.Ramos on 25/09/17.
 */

public class ShotItemPresenterTest {

    @Mock
    private ShotItemContract.View mView;

    private ShotItemContract.Interaction mPresenter;
    private Shot mResponse;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        mPresenter = new ShotItemPresenter(mView);
        mResponse = UnitTestUtil.getInstance().readFile(Shot.class, "shot.json");
    }

    @Test
    public void testHandleShot_shotNull() throws Exception {
        mPresenter.handleShot(null);
        verify(mView, never()).showImage(anyString());
        verify(mView, never()).showTitle(anyString());
        verify(mView, never()).showUserName(anyString());
        verify(mView, never()).showUserType(anyString());
    }

    @Test
    public void testHandleShot_imagesNormal() throws Exception {
        mResponse.getImages().setNormal(null);
        mPresenter.handleShot(mResponse);
        verify(mView, never()).showImage(anyString());
        verify(mView).showTitle(eq(mResponse.getTitle()));
        verify(mView).showUserName(eq(mResponse.getUser().getName()));
        verify(mView).showUserType(eq(mResponse.getUser().getType()));
    }

    @Test
    public void testHandleShot_userNull() throws Exception {
        mResponse.setUser(null);
        mPresenter.handleShot(mResponse);
        verify(mView).showImage(eq(mResponse.getImages().getNormal()));
        verify(mView).showTitle(eq(mResponse.getTitle()));
        verify(mView, never()).showUserName(anyString());
        verify(mView, never()).showUserType(anyString());
    }

    @Test
    public void testHandleShot_titleNull() throws Exception {
        mResponse.setTitle(null);
        mPresenter.handleShot(mResponse);
        verify(mView).showImage(eq(mResponse.getImages().getNormal()));
        verify(mView, never()).showTitle(anyString());
        verify(mView).showUserName(eq(mResponse.getUser().getName()));
        verify(mView).showUserType(eq(mResponse.getUser().getType()));
    }

    @Test
    public void testHandleShot_userNameNull() throws Exception {
        mResponse.getUser().setName(null);
        mPresenter.handleShot(mResponse);
        verify(mView).showImage(eq(mResponse.getImages().getNormal()));
        verify(mView).showTitle(eq(mResponse.getTitle()));
        verify(mView, never()).showUserName(anyString());
        verify(mView).showUserType(eq(mResponse.getUser().getType()));
    }

    @Test
    public void testHandleShot_userTypeNull() throws Exception {
        mResponse.getUser().setType(null);
        mPresenter.handleShot(mResponse);
        verify(mView).showImage(eq(mResponse.getImages().getNormal()));
        verify(mView).showTitle(eq(mResponse.getTitle()));
        verify(mView).showUserName(eq(mResponse.getUser().getName()));
        verify(mView, never()).showUserType(anyString());
    }
}
