package br.com.aramosdev.desafiouol.home;

import java.util.List;

import br.com.aramosdev.desafiouol.core.BaseContract;
import br.com.aramosdev.desafiouol.model.shots.Shot;

/**
 * Created by Alberto.Ramos on 24/09/17.
 */

public interface HomeContract {

    interface View extends BaseContract.BaseView {
        void tryAgain();
        void fillShotList(List<Shot> contentNewses);
    }

    interface Interaction extends BaseContract.BaseInteraction<List<Shot>> {
        void getShots(int page);
        void getShots();
        void getShotsToSort(String sort);
    }
}
