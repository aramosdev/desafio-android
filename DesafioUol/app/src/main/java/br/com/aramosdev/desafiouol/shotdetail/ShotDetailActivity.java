package br.com.aramosdev.desafiouol.shotdetail;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.com.aramosdev.desafiouol.R;
import br.com.aramosdev.desafiouol.core.BaseActivity;
import br.com.aramosdev.desafiouol.util.ImageUtil;

/**
 * Created by Alberto.Ramos on 25/09/17.
 */

public class ShotDetailActivity extends BaseActivity implements ShotDetailContract.View {

    public static final String ID_SHOT_EXTRA = "idShot";

    private ShotDetailContract.Interaction mPresenter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RelativeLayout mShareContainer;
    private ImageView mProfileImage;
    private ImageView mShotImage;
    private TextView mDescription;
    private TextView mTotalLikes;
    private TextView mTotalViews;
    private TextView mTitle;
    private TextView mAuthor;
    private long mIdShot;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_detail);
        init();
    }

    private void init() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mShareContainer = (RelativeLayout) findViewById(R.id.share_container);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);
        mDescription = (TextView) findViewById(R.id.description);
        mTotalLikes = (TextView) findViewById(R.id.total_likes);
        mTotalViews = (TextView) findViewById(R.id.total_views);
        mShotImage = (ImageView) findViewById(R.id.shot_image);
        mAuthor = (TextView) findViewById(R.id.author);
        mTitle = (TextView) findViewById(R.id.title);

        mPresenter = new ShotDetailPresenter(mApi, this);
        if(getIntent().getExtras() != null) {
            mIdShot = getIntent().getExtras().getLong(ID_SHOT_EXTRA);
        }

        mPresenter.getShotDetail(mIdShot);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getShotDetail(mIdShot);
            }
        });

        setReturnButton();
    }

    @Override
    public void showImageProfile(String image) {
        ImageUtil.loadImageCircleInto(image, mProfileImage, this);
        mProfileImage.setVisibility(View.VISIBLE);
    }

    @Override
    public void showImage(String image) {
        ImageUtil.loadImageInto(image, mShotImage, this);
        mShotImage.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTitle(String title) {
        mTitle.setText(title);
        mTitle.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDescription(String description) {
        Spanned spanned;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            spanned = Html.fromHtml(description, Html.FROM_HTML_MODE_LEGACY);
        } else {
            spanned = Html.fromHtml(description);
        }
        mDescription.setText(spanned);
        mDescription.setVisibility(View.VISIBLE);
    }

    @Override
    public void showUserName(String name) {
        mAuthor.setText(name);
        mAuthor.setVisibility(View.VISIBLE);
        findViewById(R.id.by).setVisibility(View.VISIBLE);
    }

    @Override
    public void setTotalLikes(String totalLikes) {
        mTotalLikes.setText(totalLikes);
    }

    @Override
    public void setTotalViews(String totalViews) {
        mTotalViews.setText(totalViews);
    }

    @Override
    public void setSharedProject(final String message) {
        mShareContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

                share.putExtra(Intent.EXTRA_TEXT, message);

                startActivity(Intent.createChooser(share, getString(R.string.title_shared)));
            }
        });
    }

    @Override
    public void tryAgain() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.DialogTheme);
        builder.setTitle(R.string.ops_title);
        builder.setCancelable(false);
        builder.setMessage(R.string.error_message);

        builder.setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mPresenter.getShotDetail(mIdShot);
            }
        });
        builder.setNegativeButton(R.string.cancel, null);

        builder.show();
    }

    @Override
    public void showLoading() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public Context getContext() {
        return this;
    }
}
