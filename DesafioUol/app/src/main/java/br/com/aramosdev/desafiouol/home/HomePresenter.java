package br.com.aramosdev.desafiouol.home;

import java.util.List;

import br.com.aramosdev.desafiouol.core.BasePresenter;
import br.com.aramosdev.desafiouol.model.api.RestClient;
import br.com.aramosdev.desafiouol.model.shots.Shot;
import br.com.aramosdev.desafiouol.util.TextUtils;

/**
 * Created by Alberto.Ramos on 24/09/17.
 */

public class HomePresenter extends BasePresenter<List<Shot>, HomeContract.View>
        implements HomeContract.Interaction {

    public static final int PER_PAGE_SHOT = 20;
    public static final String SORT_TO_COMMENTS = "comments";
    public static final String SORT_TO_RECENT = "recent";
    public static final String SORT_TO_VIEWS = "views";

    private int mCurrentPage;
    private String mCurrentSort;

    public HomePresenter(RestClient api, HomeContract.View view) {
        super(api, view);
    }

    @Override
    public void getShots(int page) {
        mView.showLoading();
        mCurrentPage = page;
        execute(mApi.getServices().getShots(mCurrentPage, PER_PAGE_SHOT, mCurrentSort));
    }

    @Override
    public void getShots() {
        mView.showLoading();
        execute(mApi.getServices().getShots(mCurrentPage, PER_PAGE_SHOT, mCurrentSort));
    }

    @Override
    public void getShotsToSort(String sort) {
        mView.showLoading();
        mCurrentPage = 0;
        mCurrentSort = sort;
        execute(mApi.getServices().getShots(mCurrentPage, PER_PAGE_SHOT, mCurrentSort));
    }

    @Override
    public void handleResponse(List<Shot> response) {
        mView.hideLoading();
        if (TextUtils.isEmptyOrNull(response)) return;

        mView.fillShotList(response);
    }

    @Override
    public void handleResponseError(List<Shot> response) {
        mView.hideLoading();
        mView.tryAgain();
    }
}
