package br.com.aramosdev.desafiouol.home;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import br.com.aramosdev.desafiouol.R;
import br.com.aramosdev.desafiouol.core.BaseActivity;
import br.com.aramosdev.desafiouol.model.shots.Shot;
import br.com.aramosdev.desafiouol.shotdetail.ShotDetailActivity;
import br.com.aramosdev.desafiouol.shotlist.ShotAdapter;
import br.com.aramosdev.desafiouol.util.EndlessRecyclerScrollListener;

/**
 * Created by Alberto.Ramos on 24/09/17.
 */

public class HomeActivity extends BaseActivity implements HomeContract.View {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mListShot;
    private HomeContract.Interaction mPresenter;
    private ShotAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
    }

    private void init() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mListShot = (RecyclerView) findViewById(R.id.list_shot);
        mListShot.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);

        mListShot.setLayoutManager(layoutManager);

        mPresenter = new HomePresenter(mApi, this);
        mAdapter = new ShotAdapter(this);
        mPresenter.getShots();
        mListShot.setAdapter(mAdapter);

        mListShot.addOnScrollListener(new EndlessRecyclerScrollListener(layoutManager) {
            @Override
            protected void onLoadMore(int currentPage) {
                mPresenter.getShots(currentPage);
            }
        });

        findViewById(R.id.sort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),
                        R.style.DialogTheme);
                builder.setTitle(R.string.dialog_sort_title);
                final String[] sortList = getResources().getStringArray(R.array.sort_list);
                builder.setItems(sortList, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                        String sort = sortList[i];
                        if (sort.equals(getString(R.string.sort_recent))) {
                            sort = HomePresenter.SORT_TO_RECENT;
                        } else if (sort.equals(getString(R.string.sort_comments))) {
                            sort = HomePresenter.SORT_TO_COMMENTS;
                        } else if (sort.equals(getString(R.string.sort_views))) {
                            sort = HomePresenter.SORT_TO_VIEWS;
                        }
                        mAdapter.clearItems();
                        mPresenter.getShotsToSort(sort);

                    }
                });
                builder.create().show();
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadHome();
            }
        });
    }

    @Override
    public void fillShotList(final List<Shot> shotList) {
        mAdapter.setOnItemClick(new ShotAdapter.OnItemClick() {
            @Override
            public void onItemClicked(Shot contentNews) {
                startShotDetail(contentNews.getId());
            }
        });
        mAdapter.addShot(shotList);
    }

    @Override
    public void tryAgain() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.DialogTheme);
        builder.setTitle(R.string.ops_title);
        builder.setCancelable(false);
        builder.setMessage(R.string.error_message);

        builder.setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mPresenter.getShots();
            }
        });
        builder.setNegativeButton(R.string.cancel, null);

        builder.show();
    }

    @Override
    public void showLoading() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public Context getContext() {
        return this;
    }

    private void startShotDetail(long idShot) {
        Intent intent = new Intent(this, ShotDetailActivity.class);
        intent.putExtra(ShotDetailActivity.ID_SHOT_EXTRA, idShot);
        startActivity(intent);
    }

    private void loadHome() {
        mAdapter.clearItems();
        mPresenter.getShots(0);
    }
}
