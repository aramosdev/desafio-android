package br.com.aramosdev.desafiouol.shotlist;

import br.com.aramosdev.desafiouol.model.shots.Shot;
import br.com.aramosdev.desafiouol.model.shots.User;
import br.com.aramosdev.desafiouol.util.TextUtils;

/**
 * Created by Alberto.Ramos on 24/09/17.
 */

public class ShotItemPresenter implements ShotItemContract.Interaction {

    private final ShotItemContract.View mView;

    public ShotItemPresenter(ShotItemContract.View view) {
        mView = view;
    }

    @Override
    public void handleShot(Shot shot) {
        if (shot == null) return;

        if (shot.getImages() != null && shot.getImages().getNormal() != null) {
            mView.showImage(shot.getImages().getNormal());
        }

        if (!TextUtils.isNullOrEmpty(shot.getTitle())) mView.showTitle(shot.getTitle());

        if (shot.getUser() != null) {
            User user = shot.getUser();
            if (!TextUtils.isNullOrEmpty(user.getName())) mView.showUserName(user.getName());
            if (!TextUtils.isNullOrEmpty(user.getType())) mView.showUserType(user.getType());
        }
    }
}
