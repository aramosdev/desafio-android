package br.com.aramosdev.desafiouol.shotlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.aramosdev.desafiouol.R;
import br.com.aramosdev.desafiouol.core.ViewWrapper;
import br.com.aramosdev.desafiouol.model.shots.Shot;
import br.com.aramosdev.desafiouol.util.ImageUtil;

/**
 * Created by Alberto.Ramos on 24/09/17.
 */

public class ShotItemView extends LinearLayout implements ViewWrapper.Binder<Shot>,
        ShotItemContract.View {

    private ImageView mImage;
    private TextView mTitle;
    private TextView mUserName;
    private TextView mUserType;
    private ShotItemContract.Interaction mPresenter;

    public ShotItemView(Context context) {
        super(context);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_shot_item, this, true);

        mImage = (ImageView) findViewById(R.id.shot_image);
        mTitle = (TextView) findViewById(R.id.shot_title);
        mUserName = (TextView) findViewById(R.id.user_name);
        mUserType = (TextView) findViewById(R.id.user_type);

        mPresenter = new ShotItemPresenter(this);
    }

    @Override
    public void bind(Shot data, int position) {
        mPresenter.handleShot(data);
    }

    @Override
    public void showImage(String image) {
        ImageUtil.loadImageInto(image, mImage, getContext());
        mImage.setVisibility(VISIBLE);
    }

    @Override
    public void showTitle(String title) {
        mTitle.setText(title);
        mTitle.setVisibility(VISIBLE);
    }

    @Override
    public void showUserName(String name) {
        mUserName.setText(name);
        mUserName.setVisibility(VISIBLE);
    }

    @Override
    public void showUserType(String type) {
        mUserType.setText(type);
        mUserType.setVisibility(VISIBLE);
    }
}
