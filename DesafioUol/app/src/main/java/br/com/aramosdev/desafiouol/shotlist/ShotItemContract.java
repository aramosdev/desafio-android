package br.com.aramosdev.desafiouol.shotlist;

import br.com.aramosdev.desafiouol.model.shots.Shot;

/**
 * Created by Alberto.Ramos on 24/09/17.
 */

public interface ShotItemContract {
    interface View {
        void showImage(String image);
        void showTitle(String title);
        void showUserName(String name);
        void showUserType(String type);
    }

    interface Interaction {
        void handleShot(Shot shot);
    }
}
