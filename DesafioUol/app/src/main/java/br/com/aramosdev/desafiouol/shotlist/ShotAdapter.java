package br.com.aramosdev.desafiouol.shotlist;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;

import java.util.List;

import br.com.aramosdev.desafiouol.core.BaseActivity;
import br.com.aramosdev.desafiouol.core.BaseRecyclerAdapter;
import br.com.aramosdev.desafiouol.core.ViewWrapper;
import br.com.aramosdev.desafiouol.model.shots.Shot;

/**
 * Created by Alberto.Ramos on 24/09/17.
 */

public class ShotAdapter extends BaseRecyclerAdapter<Shot, ShotItemView>{

    private final static int FADE_DURATION = 1000; // in milliseconds
    private OnItemClick mOnItemClick;


    public ShotAdapter(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    protected ShotItemView onCreateItemView(ViewGroup parent, int viewType) {
        return new ShotItemView(mActivity);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<Shot, ShotItemView> holder, final int position) {
        super.onBindViewHolder(holder, position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClick != null) mOnItemClick.onItemClicked(mItems.get(position));
            }
        });

        setAnimation(holder.itemView);
    }

    @Override
    public void onViewDetachedFromWindow(ViewWrapper<Shot, ShotItemView> holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    public void addShot(List<Shot> shots) {
        mItems.addAll(shots);
        notifyDataSetChanged();
    }

    private void setAnimation(View viewToAnimate) {
        viewToAnimate.clearAnimation();
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(FADE_DURATION);
        viewToAnimate.startAnimation(anim);
    }

    public void setOnItemClick(OnItemClick onItemClick) {
        this.mOnItemClick = onItemClick;
    }

    public interface OnItemClick {
        void onItemClicked(Shot shot);
    }
}
