package br.com.aramosdev.desafiouol.model.api;

import java.util.List;

import br.com.aramosdev.desafiouol.model.shots.Shot;
import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Alberto.Ramos on 24/09/17.
 */

public interface Services {

    @GET("shots")
    @Headers({"Authorization: Bearer 58be76c3691d16401f8f4812f12049fb2d33fee529343279d305ecb6ed739ced"})
    Flowable<List<Shot>> getShots(@Query("page") int page,
                                  @Query("per_page") int perPage,
                                  @Query("sort") String sort);

    @GET("shots/{id}")
    @Headers({"Authorization: Bearer 58be76c3691d16401f8f4812f12049fb2d33fee529343279d305ecb6ed739ced"})
    Flowable<Shot> getShots(@Path("id") long id);
}
