package br.com.aramosdev.desafiouol.shotdetail;

import br.com.aramosdev.desafiouol.core.BaseContract;
import br.com.aramosdev.desafiouol.model.shots.Shot;

/**
 * Created by Alberto.Ramos on 25/09/17.
 */

public interface ShotDetailContract {

    interface View extends BaseContract.BaseView {

        void showImageProfile(String image);
        void showTitle(String title);
        void showUserName(String name);
        void showImage(String image);
        void showDescription(String description);
        void setTotalLikes(String totalLikes);
        void setTotalViews(String totalViews);
        void tryAgain();
        void setSharedProject(String message);
    }

    interface Interaction extends BaseContract.BaseInteraction<Shot> {
        void getShotDetail(long idShot);
    }

}
