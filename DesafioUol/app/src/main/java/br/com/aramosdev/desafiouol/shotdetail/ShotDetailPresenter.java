package br.com.aramosdev.desafiouol.shotdetail;

import android.content.Context;
import android.content.res.Resources;

import br.com.aramosdev.desafiouol.R;
import br.com.aramosdev.desafiouol.core.BasePresenter;
import br.com.aramosdev.desafiouol.model.api.RestClient;
import br.com.aramosdev.desafiouol.model.shots.Shot;
import br.com.aramosdev.desafiouol.model.shots.User;
import br.com.aramosdev.desafiouol.util.TextUtils;

/**
 * Created by Alberto.Ramos on 25/09/17.
 */

public class ShotDetailPresenter extends BasePresenter<Shot, ShotDetailContract.View>
        implements ShotDetailContract.Interaction {

    public ShotDetailPresenter(RestClient api, ShotDetailContract.View view) {
        super(api, view);
    }

    @Override
    public void getShotDetail(long idShot) {
        if (idShot > 0) {
            mView.showLoading();
            execute(mApi.getServices().getShots(idShot));
        }
    }

    @Override
    public void handleResponse(Shot shot) {
        mView.hideLoading();
        if (shot == null) return;

        if (!TextUtils.isNullOrEmpty(shot.getTitle())) mView.showTitle(shot.getTitle());

        if (!TextUtils.isNullOrEmpty(shot.getDescription())) mView.showDescription(shot.getDescription());

        if (shot.getImages() != null && shot.getImages().getNormal() != null) {
            mView.showImage(shot.getImages().getHidpi());
        }

        if (shot.getUser() != null) {
            User user = shot.getUser();
            if (!TextUtils.isNullOrEmpty(user.getName())) mView.showUserName(user.getName());
            if (!TextUtils.isNullOrEmpty(user.getAvatarUrl())) {
                mView.showImageProfile(user.getAvatarUrl());
            }
        }
        Resources res = mView.getContext().getResources();
        mView.setTotalLikes(res.getQuantityString(R.plurals.likes,shot.getLikesCount(),shot.getLikesCount()));

        mView.setTotalViews(res.getQuantityString(R.plurals.views,shot.getViewsCount(),
                shot.getViewsCount()));

        Context context = mView.getContext();

        String url = shot.getHtmlUrl();
        String appName = context.getString(R.string.app_name);
        String message = context.getString(R.string.shared_message, shot.getTitle(), appName, url);

        mView.setSharedProject(message);
    }

    @Override
    public void handleResponseError(Shot response) {
        mView.hideLoading();
        mView.tryAgain();
    }
}
