package br.com.aramosdev.desafiouol.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

/**
 * Created by Alberto.Ramos on 24/09/17.
 */

public class ImageUtil {

    public static void loadImageInto(String path, ImageView imageView, Context context) {
        DrawableTypeRequest load = Glide.with(context)
                .load(path);
        load.into(imageView);
    }
    public static void loadImageCircleInto(String path,
                                           final ImageView imageView,
                                           final Context context) {
        Glide.with(context).load(path).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }
}
