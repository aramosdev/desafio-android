# Criar um aplicativo de consulta na api do [Dribbble](https://dribbble.com) #

** Desafio deve ser feito somente em Java **

Criar um aplicativo para consultar a [Dribbble API](http://developer.dribbble.com/v1/) e criar listagem de shots.

Nos envie uma solução mesmo que você não consiga fazer tudo. O teste serve pra conhecermos a sua forma de pensar, resolver problemas e seu estilo de código.

# Deve conter #

* Arquivo .gitignore
* Gestão de dependências no projeto.
* Framework para Comunicação com API.
* Lista de shots (com opções de ordenação)
* Paginação automática (scroll infinito) na tela de lista de shots
* Paginação deve detectar quando chega a última página e parar de solicitar mais
* Pull to refresh
* Tela de detalhe de um shot ao clicar em um item da lista de shots
* Tela de detalhe de um shot deve conter nome do autor, foto e descrição do shot

# Ganha + pontos se contiver #

* Testes unitários no projeto. Ex: Robolectric
* Testes funcionais. Ex: Espresso
* App Universal com suporte à Smartphone | Tablet | Landscape | Portrait (Size Classes)
* Cache de Imagens.
* Compartilhar shots no Facebook e Twitter
* Animações
* Solicitação de permissões em tempo de execução
* Utilização de CardView e RecyclerView


### **Processo de submissão** ###

O candidato deverá implementar a solução e enviar um pull request para este repositório com a solução.

O processo de Pull Request funciona da seguinte maneira:

1. Candidato fará um fork desse repositório (não irá clonar direto!)
2. Fará seu projeto nesse fork.
3. Commitará e subirá as alterações para o __SEU__ fork.
4. Pela interface do Bitbucket, irá enviar um Pull Request.

Se possível deixar o fork público para facilitar a inspeção do código.

### **ATENÇÃO** ###

Não se deve tentar fazer o PUSH diretamente para ESTE repositório!